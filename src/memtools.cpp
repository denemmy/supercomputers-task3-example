#include <cstdlib>
#include "memtools.h"

using std::size_t;
using std::malloc;

void *aligned_malloc(size_t alignment, size_t size)
{
    return malloc(size);
}
