#ifdef WITH_CUDA

const int BLOCK_SIZE = 512;

int get_grid_size(int m) {
    return (m + BLOCK_SIZE - 1) / BLOCK_SIZE;
}

__global__
void cuda_kernel_1_impl(const int m, double *p_p, double *p_c, double *p_n, int i, double *func_c, double *residual_c, double *imx_grid, double *imy_grid, double *idx_grid, double *idy_grid)
{
    int j = blockIdx.x * blockDim.x + threadIdx.x;
    if(j > 0 && j < m) {
        residual_c[j] = + imx_grid[i] * (idx_grid[i - 1] * (p_c[j] - p_p[j]) - idx_grid[i] * (p_n[j] - p_c[j]))
                        + imy_grid[j] * (idy_grid[j - 1] * (p_c[j] - p_c[j - 1]) - idy_grid[j] * (p_c[j + 1] - p_c[j]))
                        - func_c[j];
    }
}

__global__
void cuda_kernel_2_impl(const int m, double *r_p, double *r_c, double *r_n, int i, double *my_grid, double *g_c, double *imx_grid, double *imy_grid, double *idx_grid, double *idy_grid, double *output)
{
    __shared__ double partialSum[2 * BLOCK_SIZE];
    int globalThreadId = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int t = threadIdx.x;
    unsigned int start = 2*blockIdx.x * blockDim.x;

    int j = start + t;
    if(j > 0 && j < m) {
        double laplace = + imx_grid[i] * (idx_grid[i - 1] * (r_c[j] - r_p[j]) - idx_grid[i] * (r_n[j] - r_c[j]))
                         + imy_grid[j] * (idy_grid[j - 1] * (r_c[j] - r_c[j - 1]) - idy_grid[j] * (r_c[j + 1] - r_c[j]));
        partialSum[t] = my_grid[j] * laplace * g_c[j];
    } else {
        partialSum[t] = 0.0;
    }

    j = start + blockDim.x + t;
    if(j > 0 && j < m) {
        double laplace = + imx_grid[i] * (idx_grid[i - 1] * (r_c[j] - r_p[j]) - idx_grid[i] * (r_n[j] - r_c[j]))
                         + imy_grid[j] * (idy_grid[j - 1] * (r_c[j] - r_c[j - 1]) - idy_grid[j] * (r_c[j + 1] - r_c[j]));
        partialSum[blockDim.x + t] = my_grid[j] * laplace * g_c[j];
    } else {
        partialSum[blockDim.x + t] = 0.0;
    }

    for (unsigned int stride = blockDim.x; stride > 0; stride /= 2) {
        __syncthreads();
        if(t < stride) {
            partialSum[t] += partialSum[t + stride];
        }
    }
    __syncthreads();

    if(t == 0 && (globalThreadId * 2) < m) {
        output[blockIdx.x] = partialSum[t];
    }
}

__global__
void cuda_kernel_3_impl(const int m, double *r_c, double *g_c, const double alpha)
{
    int j = blockIdx.x * blockDim.x + threadIdx.x;
    if(j > 0 && j < m) {
        g_c[j] = r_c[j] - alpha * g_c[j];
    }
}

__global__
void cuda_kernel_4_impl(const int m,
                        double *g_p, double *g_c, double *g_n,
                        int i, double *my_grid, double *r_c,
                        double *imx_grid, double *imy_grid, double *idx_grid, double *idy_grid, double *out_num, double *out_denum)
{
    __shared__ double partialSum_num[2 * BLOCK_SIZE];
    __shared__ double partialSum_denum[2 * BLOCK_SIZE];
    int globalThreadId = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int t = threadIdx.x;
    unsigned int start = 2*blockIdx.x * blockDim.x;

    int j = start + t;
    if(j > 0 && j < m) {
        double cvalue = my_grid[j] * g_c[j];
        double laplace = + imx_grid[i] * (idx_grid[i - 1] * (g_c[j] - g_p[j]) - idx_grid[i] * (g_n[j] - g_c[j]))
                         + imy_grid[j] * (idy_grid[j - 1] * (g_c[j] - g_c[j - 1]) - idy_grid[j] * (g_c[j + 1] - g_c[j]));
        partialSum_num[t] = cvalue * r_c[j];
        partialSum_denum[t] = cvalue * laplace;
    } else {
        partialSum_num[t] = 0.0;
        partialSum_denum[t] = 0.0;
    }

    j = start + blockDim.x + t;
    if(j > 0 && j < m) {
        double cvalue = my_grid[j] * g_c[j];
        double laplace = + imx_grid[i] * (idx_grid[i - 1] * (g_c[j] - g_p[j]) - idx_grid[i] * (g_n[j] - g_c[j]))
                         + imy_grid[j] * (idy_grid[j - 1] * (g_c[j] - g_c[j - 1]) - idy_grid[j] * (g_c[j + 1] - g_c[j]));
        partialSum_num[blockDim.x + t] = cvalue * r_c[j];
        partialSum_denum[blockDim.x + t] = cvalue * laplace;
    } else {
        partialSum_num[blockDim.x + t] = 0.0;
        partialSum_denum[blockDim.x + t] = 0.0;
    }

    for (unsigned int stride = blockDim.x; stride > 0; stride /= 2) {
        __syncthreads();
        if(t < stride) {
            partialSum_num[t] += partialSum_num[t + stride];
            partialSum_denum[t] += partialSum_denum[t + stride];
        }
    }
    __syncthreads();

    if(t == 0 && (globalThreadId * 2) < m) {
        out_num[blockIdx.x] = partialSum_num[t];
        out_denum[blockIdx.x] = partialSum_denum[t];
    }
}

__global__
void cuda_kernel_5_impl(int m, const double tau, double *g_c, double *p_c, double *my_grid, double *out_norm)
{
    __shared__ double partialSum[2 * BLOCK_SIZE];
    int globalThreadId = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int t = threadIdx.x;
    unsigned int start = 2*blockIdx.x * blockDim.x;

    int j = start + t;
    if(j > 0 && j < m) {
        double diff = tau * g_c[j];
        p_c[j] = p_c[j] - diff;
        partialSum[t] = my_grid[j] * diff * diff;
    } else {
        partialSum[t] = 0.0;
    }

    j = start + blockDim.x + t;
    if(j > 0 && j < m) {
        double diff = tau * g_c[j];
        p_c[j] = p_c[j] - diff;
        partialSum[blockDim.x + t] = my_grid[j] * diff * diff;
    } else {
        partialSum[blockDim.x + t] = 0.0;
    }

    for (unsigned int stride = blockDim.x; stride > 0; stride /= 2) {
        __syncthreads();
        if(t < stride) {
            partialSum[t] += partialSum[t + stride];
        }
    }
    __syncthreads();

    if(t == 0 && (globalThreadId * 2) < m) {
        out_norm[blockIdx.x] = partialSum[t];
    }
}

void cuda_kernel_1(int m, double *p_p, double *p_c, double *p_n, int i, double *func_c, double *residual_c, double *imx_grid, double *imy_grid, double *idx_grid, double *idy_grid)
{
    cuda_kernel_1_impl<<<get_grid_size(m), BLOCK_SIZE>>>(m, p_p, p_c, p_n, i, func_c, residual_c, imx_grid, imy_grid, idx_grid, idy_grid);
}

void cuda_kernel_2(int m, double *r_p, double *r_c, double *r_n, int i, double *my_grid, double *g_c, double *imx_grid, double *imy_grid, double *idx_grid, double *idy_grid, double *output)
{
    cuda_kernel_2_impl<<<get_grid_size(m), BLOCK_SIZE>>>(m, r_p, r_c, r_n, i, my_grid, g_c, imx_grid, imy_grid, idx_grid, idy_grid, output);
}

void cuda_kernel_3(int m, double *r_c, double *g_c, const double alpha)
{
    cuda_kernel_3_impl<<<get_grid_size(m), BLOCK_SIZE>>>(m, r_c, g_c, alpha);
}

void cuda_kernel_4(int m,
                   double *g_p, double *g_c, double *g_n,
                   int i, double *my_grid, double *r_c,
                   double *imx_grid, double *imy_grid, double *idx_grid, double *idy_grid,
                   double *out_num, double *out_denum)
{
    cuda_kernel_4_impl<<<get_grid_size(m), BLOCK_SIZE>>>(m, g_p, g_c, g_n, i, my_grid, r_c, imx_grid, imy_grid, idx_grid, idy_grid, out_num, out_denum);
}

void cuda_kernel_5(int m, const double tau, double *g_c, double *p_c, double *my_grid, double *out_norm)
{
    cuda_kernel_5_impl<<<get_grid_size(m), BLOCK_SIZE>>>(m, tau, g_c, p_c, my_grid, out_norm);
}

#endif
