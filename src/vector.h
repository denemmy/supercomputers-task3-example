#pragma once

#include <string>

#if __cplusplus > 199711L
 #include <cstdint>
#else
 #define noexcept throw()
 #define override
 #define nullptr NULL
#endif

class VectorError: public std::exception {
    std::string _what;
public:
    VectorError(const std::string &what): _what(what) {}
#if __cplusplus > 199711L
    ~VectorError() = default;
#else
    ~VectorError() noexcept;
#endif
    const char * what() const noexcept override { return _what.c_str(); }
};

class Vector
{
#ifdef WITH_CUDA
    double *_gpu_data;
    double *_cpu_data;
#endif
    double *_data;
    std::size_t _n;
protected:
    bool check_dims(const Vector &p) const noexcept;
    void validate_dims(const Vector &p) const;
public:
    Vector();
    Vector(std::size_t n, const double default_value = 0.0);
    virtual ~Vector();

    Vector(const Vector &p);
#if __cplusplus > 199711L
    Vector(Vector &&p);
#endif
    void swap(Vector &p) noexcept;

    Vector & operator = (const Vector &p);
#if __cplusplus > 199711L
    Vector & operator = (Vector &&p);
#endif

    const size_t n() const { return _n; }
    std::size_t size() const { return _n; }

    const double * data() const { return _data; }
    double * data() { return _data; }

    const double operator [](const std::size_t i) const { return _data[i]; }
    double & operator [](const std::size_t i) { return _data[i]; }

#ifdef WITH_CUDA
    bool is_gpu() const { return _data == _gpu_data; };
    bool is_cpu() const { return _data == _cpu_data; };
    void to_gpu(bool copy = true);
    void to_cpu(bool copy = true);
    double * gpu_data() { return _gpu_data; }
    double * cpu_data() { return _cpu_data; }
#endif
};
