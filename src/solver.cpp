#include <cstdio>

#include "solver.h"

#include <cmath>
#include <limits>
#include <ctime>

#ifdef WITH_MPI
#include <mpi.h>
#endif

#ifdef WITH_CUDA
#include <cuda_runtime.h>
#endif

#if defined(WITH_CUDA) && defined(WITH_OPENMP)
    #pragma error("WITH_CUDA and WITH_OPENMP is unsupported configuration")
#endif

using std::size_t;
using std::vector;
using std::pow;
using std::sqrt;
using std::abs;
using std::sqrt;

#if __cplusplus > 199711L
const double NaN = std::numeric_limits<double>::quiet_NaN();
#else
const double NaN = NAN;
#endif

#if __cplusplus <= 199711L
SolverError::~SolverError() noexcept {
}
#endif

Vector GridSolver::build_grid(size_t n, double e1, double e2) const
{
    Vector points(n + 1);

    for(size_t i = 0; i <= n; ++i) {
        double f_i = func_mapping(double(i) / n);
        points[i] = e1 * (1.0 - f_i) + e2 * f_i;
    }

    return points;
}

double GridSolver::laplace(const Grid &p, size_t i, size_t j)
{
    return
        1.0 / _mx_grid[i] * ((p[i][j] - p[i - 1][j]) / _dx_grid[i - 1] - (p[i + 1][j] - p[i][j]) / _dx_grid[i]) +
        1.0 / _my_grid[j] * ((p[i][j] - p[i][j - 1]) / _dy_grid[j - 1] - (p[i][j + 1] - p[i][j]) / _dy_grid[j]);
}

#if __cplusplus <= 199711L
double GridSolver::laplace_lambda(const double *p_p, const double *p_c, const double *p_n, const size_t i, const size_t j)
{
    const double *imx_grid = &_imx_grid[0];
    const double *imy_grid = &_imy_grid[0];
    const double *idx_grid = &_idx_grid[0];
    const double *idy_grid = &_idy_grid[0];

    return imx_grid[i] * (idx_grid[i - 1] * (p_c[j] - p_p[j]) - idx_grid[i] * (p_n[j] - p_c[j])) + 
           imy_grid[j] * (idy_grid[j - 1] * (p_c[j] - p_c[j - 1]) - idy_grid[j] * (p_c[j + 1] - p_c[j]));
}
#endif

double GridSolver::func_phi(double x, double y) const
{
    double dx = 1.0 - x * x;
    double dy = 1.0 - y * y;
    return dx * dx + dy * dy;
}

double GridSolver::func_f(double x, double y) const
{
    return 4.0 * (2.0 - 3.0 * (x * x + y * y));
}

double GridSolver::func_mapping(double t) const
{
    return (pow(1 + t, 1.5) - 1.0) / (pow(2, 1.5) - 1.0);
}

GridSolver::GridSolver(size_t n, size_t m, double x1, double x2, double y1, double y2):
    _n(n), _m(m)
{
    _x_grid = build_grid(n, x1, x2);
    _dx_grid = Vector(n + 1, NaN);
    _idx_grid = Vector(n + 1, NaN);
    for(size_t i = 0; i < n; ++i) {
        _dx_grid[i] = _x_grid[i + 1] - _x_grid[i];
        _idx_grid[i] = 1.0 / _dx_grid[i];
    }
    _mx_grid = Vector(n + 1, NaN);
    _imx_grid = Vector(n + 1, NaN);
    for(size_t i = 1; i < n; ++i) {
        _mx_grid[i] = 0.5 * (_dx_grid[i - 1] + _dx_grid[i]);
        _imx_grid[i] = 1.0 / _mx_grid[i];
    }

    _y_grid = build_grid(m, y1, y2);
    _dy_grid = Vector(m + 1, NaN);
    _idy_grid = Vector(m + 1, NaN);
    for(size_t j = 0; j < m; ++j) {
        _dy_grid[j] = _y_grid[j + 1] - _y_grid[j];
        _idy_grid[j] = 1.0 / _dy_grid[j];
    }
    _my_grid = Vector(m + 1, NaN);
    _imy_grid = Vector(m + 1, NaN);
    for(size_t j = 1; j < m; ++j) {
        _my_grid[j] = 0.5 * (_dy_grid[j - 1] + _dy_grid[j]);
        _imy_grid[j] = 1.0 / _my_grid[j];
    }

#ifdef WITH_CUDA
    _x_grid.to_gpu(); _x_grid.to_cpu(false);
    _dx_grid.to_gpu(); _dx_grid.to_cpu(false);
    _idx_grid.to_gpu(); _idx_grid.to_cpu(false);
    _mx_grid.to_gpu(); _mx_grid.to_cpu(false);
    _imx_grid.to_gpu(); _imx_grid.to_cpu(false);

    _y_grid.to_gpu(); _y_grid.to_cpu(false);
    _dy_grid.to_gpu(); _dy_grid.to_cpu(false);
    _idy_grid.to_gpu(); _idy_grid.to_cpu(false);
    _my_grid.to_gpu(); _my_grid.to_cpu(false);
    _imy_grid.to_gpu(); _imy_grid.to_cpu(false);
#endif
}

GridSolver::~GridSolver()
{
}

Grid GridSolver::solve(const double eps)
{
    Grid solution(_n + 1, _m + 1, 0.0);
    Grid residual(_n + 1, _m + 1, 0.0);
    Grid func_cache(_n + 1, _m + 1);

#ifdef WITH_MPI
    int mpi_rank, mpi_nprocs;
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_nprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    if(mpi_rank == 0) {
#endif
    for(size_t i = 0; i <= _n; ++i) {
        for(size_t j = 0; j <= _m; ++j) {
            func_cache[i][j] = func_f(_x_grid[i], _y_grid[j]);
            if(i == 0 || i == _n || j == 0 || j == _m) {
                solution[i][j] = func_phi(_x_grid[i], _y_grid[j]);
            }
        }
    }

    for(size_t i = 0; i < 10; ++i) {
        for(size_t i = 1; i < _n; ++i) {
            for(size_t j = 1; j < _m; ++j) {
                residual[i][j] = laplace(solution, i, j) - func_cache[i][j];
            }
        }

        double tau_num = 0.0;
        double tau_denum = 0.0;

        for(size_t i = 1; i < _n; ++i) {
            double tau_cnum = 0;
            double tau_cdenum = 0;
            for(size_t j = 1; j < _m; ++j) {
                tau_cnum += _mx_grid[i] * _my_grid[j] * residual[i][j] * residual[i][j];
                tau_cdenum += _mx_grid[i] * _my_grid[j] * laplace(residual, i, j) * residual[i][j];
            }
            tau_num += tau_cnum;
            tau_denum += tau_cdenum;
        }

        double tau = tau_num / tau_denum;

        for(size_t i = 1; i < _n; ++i) {
            for(size_t j = 1; j < _m; ++j) {
                solution[i][j] = solution[i][j] - tau * residual[i][j];
            }
        }
    }
#ifdef WITH_MPI
    }
    MPI_Bcast(solution.data(), solution.size(), MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(residual.data(), residual.size(), MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(func_cache.data(), func_cache.size(), MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);
#endif

    Grid g_prev = residual;

    double alpha_denum = 0.0;
    for(size_t i = 1; i < _n; ++i) {
        double alpha_cdenum = 0.0;
        for(size_t j = 1; j < _m; ++j) {
            alpha_cdenum += _mx_grid[i] * _my_grid[j] * laplace(g_prev, i, j) * g_prev[i][j];
        }
        alpha_denum += alpha_cdenum;
    }

#define ROWS(prefix, storage, i) \
    double *prefix##_p = storage[i - 1]; \
    double *prefix##_c = storage[i]; \
    double *prefix##_n = storage[i + 1]

#define MPI_SEND_CASCADE(variable) \
do { \
    if(mpi_nprocs > 1) { \
        if(mpi_rank == 0) { \
            MPI_Send(variable[e_i - 1], variable.m(), MPI_DOUBLE, 1, 0, MPI_COMM_WORLD); \
            MPI_Recv(variable[e_i], variable.m(), MPI_DOUBLE, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE); \
        } else if(mpi_rank == mpi_nprocs - 1) { \
            MPI_Recv(variable[b_i - 1], variable.m(), MPI_DOUBLE, mpi_rank - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE); \
            MPI_Send(variable[b_i], variable.m(), MPI_DOUBLE, mpi_rank - 1, 0, MPI_COMM_WORLD); \
        } else { \
            MPI_Recv(variable[b_i - 1], variable.m(), MPI_DOUBLE, mpi_rank - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE); \
            MPI_Send(variable[b_i], variable.m(), MPI_DOUBLE, mpi_rank - 1, 0, MPI_COMM_WORLD); \
            MPI_Send(variable[e_i - 1], variable.m(), MPI_DOUBLE, mpi_rank + 1, 0, MPI_COMM_WORLD); \
            MPI_Recv(variable[e_i], variable.m(), MPI_DOUBLE, mpi_rank + 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE); \
        } \
    } \
} while(0)

#ifdef WITH_CUDA
    double *mx_grid = _mx_grid.cpu_data();
    double *imx_grid = _imx_grid.cpu_data();
    double *my_grid = _my_grid.cpu_data();
    double *imy_grid = _imy_grid.cpu_data();

    double *dx_grid = _dx_grid.cpu_data();
    double *idx_grid = _idx_grid.cpu_data();
    double *dy_grid = _dy_grid.cpu_data();
    double *idy_grid = _idy_grid.cpu_data();

    double *mx_grid_g = _mx_grid.gpu_data();
    double *imx_grid_g = _imx_grid.gpu_data();
    double *my_grid_g = _my_grid.gpu_data();
    double *imy_grid_g = _imy_grid.gpu_data();

    double *dx_grid_g = _dx_grid.gpu_data();
    double *idx_grid_g = _idx_grid.gpu_data();
    double *dy_grid_g = _dy_grid.gpu_data();
    double *idy_grid_g = _idy_grid.gpu_data();
#else
    double *mx_grid = _mx_grid.data();
    double *imx_grid = _imx_grid.data();
    double *my_grid = _my_grid.data();
    double *imy_grid = _imy_grid.data();

    double *dx_grid = _dx_grid.data();
    double *idx_grid = _idx_grid.data();
    double *dy_grid = _dy_grid.data();
    double *idy_grid = _idy_grid.data();
#endif

#if __cplusplus > 199711L
    auto laplace_lambda = [&](const double *p_p, const double *p_c, const double *p_n, const size_t i, const size_t j) -> double {
        return
            imx_grid[i] * (idx_grid[i - 1] * (p_c[j] - p_p[j]) - idx_grid[i] * (p_n[j] - p_c[j])) +
            imy_grid[j] * (idy_grid[j - 1] * (p_c[j] - p_c[j - 1]) - idy_grid[j] * (p_c[j + 1] - p_c[j]));
    };
#endif

#ifdef WITH_MPI
    size_t b_i = mpi_rank * _n / mpi_nprocs;
    size_t e_i = (mpi_rank + 1) * _n / mpi_nprocs;
    if(b_i == 0) {
        b_i = 1;
    }
#else
    size_t b_i = 1;
    size_t e_i = _n;
#endif
#ifdef WITH_CUDA
    int cuda_grid_size = get_grid_size(_m);
    Grid cuda_kernel_2_grid(_n, cuda_grid_size);
    Grid cuda_kernel_4_grid_num(_n, cuda_grid_size);
    Grid cuda_kernel_4_grid_denum(_n, cuda_grid_size);
    Grid cuda_kernel_5_grid(_n, cuda_grid_size);
#endif
    while(true) {
#ifdef WITH_CUDA
        residual.to_gpu();
        func_cache.to_gpu();
        solution.to_gpu();
#endif
#ifdef WITH_OPENMP
        #pragma omp parallel for schedule(dynamic)
#endif
        for(size_t i = b_i; i < e_i; ++i) {
            ROWS(residual, residual, i);
            ROWS(func, func_cache, i);
            ROWS(p, solution, i);

#ifdef WITH_CUDA
            cuda_kernel_1(_m, p_p, p_c, p_n, i, func_c, residual_c, imx_grid_g, imy_grid_g, idx_grid_g, idy_grid_g);
#else
            for(size_t j = 1; j < _m; ++j) {
                residual_c[j] = laplace_lambda(p_p, p_c, p_n, i, j) - func_c[j];
            }
#endif
        }

#ifdef WITH_CUDA
        if(cudaDeviceSynchronize() != cudaSuccess) {
            throw SolverError("kernel 1 failed");
        }
        residual.to_cpu();
#endif

#ifdef WITH_MPI
        MPI_SEND_CASCADE(residual);
        MPI_Barrier(MPI_COMM_WORLD);
#endif
        double alpha_num = 0.0;
#ifdef WITH_CUDA
        residual.to_gpu();
        g_prev.to_gpu();
        cuda_kernel_2_grid.to_gpu(false);
#endif
#ifdef WITH_OPENMP
        #pragma omp parallel for reduction(+:alpha_num) schedule(dynamic)
#endif
        for(size_t i = b_i; i < e_i; ++i) {
            ROWS(r, residual, i);
            ROWS(g, g_prev, i);
#ifdef WITH_CUDA
            cuda_kernel_2(_m, r_p, r_c, r_n, i, my_grid_g, g_c, imx_grid_g, imy_grid_g, idx_grid_g, idy_grid_g, cuda_kernel_2_grid[i]);
#else
            double alpha_cnum = 0.0;
            for(size_t j = 1; j < _m; ++j) {
                alpha_cnum += my_grid[j] * laplace_lambda(r_p, r_c, r_n, i, j) * g_c[j];
            }
            alpha_num += mx_grid[i] * alpha_cnum;
#endif
        }
#ifdef WITH_CUDA
        if(cudaDeviceSynchronize() != cudaSuccess) {
            throw SolverError("kernel 2 failed");
        }
        cuda_kernel_2_grid.to_cpu();
        for(size_t i = b_i; i < e_i; ++i) {
            for(int j = 0; j < cuda_grid_size; ++j) {
                alpha_num += mx_grid[i] * cuda_kernel_2_grid[i][j];
            }
        }
#endif
#ifdef WITH_MPI
        MPI_Allreduce(MPI_IN_PLACE, &alpha_num, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        MPI_Barrier(MPI_COMM_WORLD);
#endif
        double alpha = alpha_num / alpha_denum;

#ifdef WITH_CUDA
        g_prev.to_gpu();
        residual.to_gpu();
#endif
#ifdef WITH_OPENMP
        #pragma omp parallel for schedule(dynamic)
#endif
        for(size_t i = b_i; i < e_i; ++i) {
            ROWS(g, g_prev, i);
            ROWS(r, residual, i);
#ifdef WITH_CUDA
            cuda_kernel_3(_m, r_c, g_c, alpha);
#else
            for(size_t j = 1; j < _m; ++j) {
                g_c[j] = r_c[j] - alpha * g_c[j];
            }
#endif
        }
#ifdef WITH_CUDA
        if(cudaDeviceSynchronize() != cudaSuccess) {
            throw SolverError("kernel 3 failed");
        }
        g_prev.to_cpu();
#endif
#ifdef WITH_MPI
        MPI_SEND_CASCADE(g_prev);
        MPI_Barrier(MPI_COMM_WORLD);
#endif

        double tau_num = 0.0;
        double tau_denum = 0.0;
#ifdef WITH_CUDA
        residual.to_gpu();
        g_prev.to_gpu();
        cuda_kernel_4_grid_num.to_gpu(false);
        cuda_kernel_4_grid_denum.to_gpu(false);
#endif
#ifdef WITH_OPENMP
        #pragma omp parallel for reduction(+:tau_num,tau_denum) schedule(dynamic)
#endif
        for(size_t i = b_i; i < e_i; ++i) {
            ROWS(r, residual, i);
            ROWS(g, g_prev, i);
#ifdef WITH_CUDA
            cuda_kernel_4(_m, g_p, g_c, g_n, i, my_grid_g, r_c, imx_grid_g, imy_grid_g, idx_grid_g, idy_grid_g, cuda_kernel_4_grid_num[i], cuda_kernel_4_grid_denum[i]);
#else
            double tau_cnum = 0;
            double tau_cdenum = 0;
            for(size_t j = 1; j < _m; ++j) {
                double cvalue = my_grid[j] * g_c[j];
                tau_cnum += cvalue * r_c[j];
                tau_cdenum += cvalue * laplace_lambda(g_p, g_c, g_n, i, j);
            }
            tau_num += mx_grid[i] * tau_cnum;
            tau_denum += mx_grid[i] * tau_cdenum;
#endif
        }
#ifdef WITH_CUDA
        if(cudaDeviceSynchronize() != cudaSuccess) {
            throw SolverError("kernel 4 failed");
        }
        cuda_kernel_4_grid_num.to_cpu();
        cuda_kernel_4_grid_denum.to_cpu();
        for(size_t i = b_i; i < e_i; ++i) {
            for(int j = 0; j < cuda_grid_size; ++j) {
                tau_num += mx_grid[i] * cuda_kernel_4_grid_num[i][j];
                tau_denum += mx_grid[i] * cuda_kernel_4_grid_denum[i][j];
            }
        }
#endif
#ifdef WITH_MPI
        MPI_Allreduce(MPI_IN_PLACE, &tau_num, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        MPI_Allreduce(MPI_IN_PLACE, &tau_denum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        MPI_Barrier(MPI_COMM_WORLD);
#endif
        alpha_denum = tau_denum;
        double tau = tau_num / tau_denum;

        double norm = 0.0;
#ifdef WITH_CUDA
        solution.to_gpu();
        g_prev.to_gpu();
        cuda_kernel_5_grid.to_gpu(false);
#endif
#ifdef WITH_OPENMP
        #pragma omp parallel for reduction(+:norm) schedule(dynamic)
#endif
        for(size_t i = b_i; i < e_i; ++i) {
            ROWS(p, solution, i);
            ROWS(g, g_prev, i);
#ifdef WITH_CUDA
            cuda_kernel_5(_m, tau, g_c, p_c, my_grid_g, cuda_kernel_5_grid[i]);
#else
            double cnorm = 0.0;
            for(size_t j = 1; j < _m; ++j) {
                double diff = tau * g_c[j];
                p_c[j] = p_c[j] - diff;
                cnorm += my_grid[j] * diff * diff;
            }
            norm += mx_grid[i] * cnorm;
#endif
        }
#ifdef WITH_CUDA
        if(cudaDeviceSynchronize() != cudaSuccess) {
            throw SolverError("kernel 5 failed");
        }
        cuda_kernel_5_grid.to_cpu();
        for(size_t i = b_i; i < e_i; ++i) {
            for(int j = 0; j < cuda_grid_size; ++j) {
                norm += mx_grid[i] * cuda_kernel_5_grid[i][j];
            }
        }
        solution.to_cpu();
#endif
#ifdef WITH_MPI
        MPI_SEND_CASCADE(solution);
        MPI_Allreduce(MPI_IN_PLACE, &norm, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        MPI_Barrier(MPI_COMM_WORLD);
#endif
        norm = std::sqrt(norm);

#ifdef WITH_DEBUG
        printf("norm is %lf\n", norm);
#endif

        if(norm < eps) {
            break;
        }

#ifdef WITH_PLOT
#ifdef WITH_MPI
        if(mpi_rank == 0) {
#endif
        FILE * fout = fopen("plot_decay.txt", "a");
        fprintf(fout, "%.10lf %.10lf\n", norm, error(solution));
        fclose(fout);
#ifdef WITH_MPI
        }
#endif
#endif

        //printf("norm is %lf\n", norm);
        //printf("residual: %.5f\n", error(solution));
    }

#ifdef WITH_MPI
    if(mpi_rank == 0) {
        for(size_t i = e_i; i < _n; ++i) {
            MPI_Recv(solution[i], solution.m(), MPI_DOUBLE, MPI_ANY_SOURCE, (int)i, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
    } else {
        for(size_t i = b_i; i < e_i; ++i) {
            MPI_Send(solution[i], solution.m(), MPI_DOUBLE, 0, i, MPI_COMM_WORLD);
        }
    }
#endif

    return solution;
}

double GridSolver::error(const Grid &u) const
{
    double norm = 0.0;

    for(size_t i = 1; i < _n; ++i) {
        double cnorm = 0.0;
        for(size_t j = 1; j < _m; ++j) {
            cnorm += _mx_grid[i] * _my_grid[j] * (u[i][j] - func_phi(_x_grid[i], _y_grid[j])) * (u[i][j] - func_phi(_x_grid[i], _y_grid[j]));
        }
        norm += cnorm;
    }

    return sqrt(norm);
}
