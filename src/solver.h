#pragma once

#include <functional>
#include <vector>

#include "grid.h"
#include "vector.h"

#if __cplusplus > 199711L
 #include <cstdint>
#else
 #define noexcept throw()
 #define override
 #define nullptr NULL
#endif

#ifdef WITH_CUDA
int get_grid_size(int m);
void cuda_kernel_1(int m, double *p_p, double *p_c, double *p_n, int i, double *func_c, double *residual_c, double *imx_grid, double *imy_grid, double *idx_grid, double *idy_grid);
void cuda_kernel_2(int m, double *r_p, double *r_c, double *r_n, int i, double *my_grid, double *g_c, double *imx_grid, double *imy_grid, double *idx_grid, double *idy_grid, double *output);
void cuda_kernel_3(int m, double *r_c, double *g_c, const double alpha);
void cuda_kernel_4(int m,
                   double *g_p, double *g_c, double *g_n,
                   int i, double *my_grid, double *r_c,
                   double *imx_grid, double *imy_grid, double *idx_grid, double *idy_grid,
                   double *out_num, double *out_denum
);
void cuda_kernel_5(int m, const double tau, double *g_c, double *p_c, double *my_grid, double *out_norm);
#endif

class SolverError: public std::exception {
    std::string _what;
public:
    SolverError(const std::string &what): _what(what) {}
#if __cplusplus > 199711L
    ~SolverError() = default;
#else
    ~SolverError() noexcept;
#endif
    const char * what() const noexcept override { return _what.c_str(); }
};

class GridSolver
{
    size_t _n, _m;
    Vector _x_grid;
    Vector _y_grid;

    Vector _dx_grid;
    Vector _dy_grid;

    Vector _mx_grid;
    Vector _my_grid;

    Vector _idx_grid;
    Vector _idy_grid;

    Vector _imx_grid;
    Vector _imy_grid;

    Grid _func_cache;
protected:
    Vector build_grid(const std::size_t n, double e1, double e2) const;
    double laplace(const Grid &p, size_t i, size_t j);
#if __cplusplus <= 199711L
    double laplace_lambda(const double *p_p, const double *p_c, const double *p_n, const std::size_t i, const std::size_t j);
#endif
public:
    virtual double func_phi(double x, double y) const;
    virtual double func_f(double x, double y) const;
    virtual double func_mapping(double t) const;
public:
    GridSolver(
        std::size_t n, std::size_t m,
        double x1 = 0.0, double x2 = 1.0,
        double y1 = 0.0, double y2 = 1.0
    );
    virtual ~GridSolver();

    const Vector & x_grid() const { return _x_grid; };
    const Vector & y_grid() const { return _y_grid; };

    Grid solve(const double eps = 1e-4);
    double error(const Grid &u) const;
};
