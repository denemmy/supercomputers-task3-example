#pragma once

#include <cstddef>

void *aligned_malloc(std::size_t alignment, std::size_t size);
