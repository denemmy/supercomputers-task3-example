#include <iostream>
#include <fstream>
#include <cmath>
#include "solver.h"

#ifdef WITH_MPI
#include <mpi.h>
#endif

using std::pow;
using std::max;
using std::abs;

int main(int argc, char **argv, char **envp)
{
#ifdef WITH_MPI
    int mpi_rank, mpi_nprocs;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_nprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    printf("MPI: starting %d of %d\n", mpi_rank, mpi_nprocs);
    double mpi_start_time = MPI_Wtime();
#endif
    GridSolver solver(1000, 1000, 0.0, 1.0, 0.0, 1.0);
    Grid u = solver.solve(1e-4);
#ifdef WITH_MPI
    if(mpi_rank == 0) {
#endif
    printf("error: %.5f\n", solver.error(u));
#ifdef WITH_MPI
    printf("time: %.5lf\n", (MPI_Wtime() - mpi_start_time) / MPI_Wtick());
    }
#endif

#ifdef WITH_MPI
    if(mpi_rank == 0) {
#endif
#ifdef WITH_PLOT
    std::ofstream fout_u("plot_u.txt");
    std::ofstream fout_residual("plot_residual.txt");
    fout_u.precision(10);
    fout_residual.precision(10);
    size_t x_step = max(size_t(1), solver.x_grid().size() / 100);
    size_t y_step = max(size_t(1), solver.y_grid().size() / 100);
    for(size_t i = 0; i < solver.x_grid().size(); i += x_step) {
        for(size_t j = 0; j < solver.y_grid().size(); j += y_step) {
            double v = solver.func_phi(solver.x_grid()[i], solver.y_grid()[j]);
            double diff = abs(v - u[i][j]);
            fout_u << solver.x_grid()[i] << " " << solver.y_grid()[j] << " " << u[i][j] << std::endl;
            fout_residual << solver.x_grid()[i] << " " << solver.y_grid()[j] << " " << diff << std::endl;
        }
    }
#endif
#ifdef WITH_MPI
    }
#endif

#ifdef WITH_MPI
    MPI_Finalize();
#endif
    return 0;
}
