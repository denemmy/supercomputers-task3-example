#include "grid.h"
#include "memtools.h"

#include <cstdlib>
#include <cstring>

#ifdef WITH_CUDA
#include <cuda_runtime.h>
#endif

using std::size_t;
using std::free;

#if __cplusplus <= 199711L
GridError::~GridError() noexcept {
}
#endif

bool Grid::check_dims(const Grid &p) const noexcept
{
    return _n == p._n && _m == p._m;
}

void Grid::validate_dims(const Grid &p) const
{
    if(!check_dims(p)) {
        throw GridError("size mismatch");
    }
}

Grid::Grid():
    _data(nullptr), _n(0), _m(0)
{
#ifdef WITH_CUDA
    _cpu_data = nullptr;
    _gpu_data = nullptr;
#endif
}

Grid::Grid(size_t n, size_t m, const double default_value):
    _data(nullptr), _n(n), _m(m)
{
#ifdef WITH_CUDA
    if(cudaMalloc(&_gpu_data, _n * _m * sizeof(_data[0])) != cudaSuccess || cudaDeviceSynchronize() != cudaSuccess) {
        throw GridError("cuda allocation failure");
    }
#endif
    _data = (double *)aligned_malloc(32, _n * _m * sizeof(_data[0]));
    for(size_t i = 0; i < _n; ++i) {
        double *row = _data + i * _m;
        for(size_t j = 0; j < _m; ++j) {
            row[j] = default_value;
        }
    }
#ifdef WITH_CUDA
    _cpu_data = _data;
    to_gpu();
    _data = _cpu_data;
#endif
}

Grid::~Grid()
{
    if(_data) {
#ifdef WITH_CUDA
        cudaDeviceSynchronize();
        cudaFree(_gpu_data);
        _gpu_data = nullptr;
        _data = _cpu_data;
        _cpu_data = nullptr;
#endif
        free(_data);
        _data = nullptr;
    }
}

Grid::Grid(const Grid &p):
    _n(p._n), _m(p._m), _data(nullptr)
{
#ifdef WITH_CUDA
    if(cudaMalloc(&_gpu_data, _n * _m * sizeof(_data[0])) != cudaSuccess || cudaDeviceSynchronize() != cudaSuccess) {
        throw GridError("cuda allocation failure");
    }
    if(cudaMemcpy(_gpu_data, p._gpu_data, _n * _m * sizeof(_data[0]), cudaMemcpyDeviceToDevice) != cudaSuccess || cudaDeviceSynchronize() != cudaSuccess) {
        throw GridError("cuda memcpy failure");
    }
    _cpu_data = (double *)aligned_malloc(32, _n * _m * sizeof(_data[0]));
    memcpy(_cpu_data, p._cpu_data, _n * _m * sizeof(_data[0]));
    if(p.is_cpu()) {
        _data = _cpu_data;
    } else {
        _data = _gpu_data;
    }
#else
    _data = (double *)aligned_malloc(32, _n * _m * sizeof(_data[0]));
    memcpy(_data, p._data, _n * _m * sizeof(_data[0]));
#endif
}

#if __cplusplus > 199711L
Grid::Grid(Grid &&p):
    _n(p._n), _m(p._m), _data(p._data)
{
    p._n = p._m = 0;
    p._data = nullptr;
#ifdef WITH_CUDA
    _gpu_data = p._gpu_data;
    _cpu_data = p._cpu_data;
    p._gpu_data = nullptr;
    p._cpu_data = nullptr;
#endif
}
#endif

void Grid::swap(Grid &p) noexcept
{
    std::swap(_n, p._n);
    std::swap(_m, p._m);
    std::swap(_data, p._data);
#ifdef WITH_CUDA
    std::swap(_gpu_data, p._gpu_data);
    std::swap(_cpu_data, p._cpu_data);
#endif
}

Grid & Grid::operator = (const Grid &p)
{
    Grid tmp(p);
    this->swap(tmp);
    return *this;
}

#if __cplusplus > 199711L
Grid & Grid::operator = (Grid &&p)
{
    this->swap(p);
    return *this;
}
#endif

Grid & Grid::operator += (const Grid &p)
{
#ifdef WITH_CUDA
    if(!is_cpu() ) {
        throw GridError("+= is enabled onlin in cpu mode");
    }
#endif

    validate_dims(p);

    for(size_t i = 0; i < _n; ++i) {
        double *drow = _data + i * _m;
        const double *srow = p._data + i * _m;
        for(size_t j = 0; j < _m; ++j) {
            drow[j] += srow[j];
        }
    }

    return *this;
}

Grid & Grid::operator -= (const Grid &p)
{
#ifdef WITH_CUDA
    if(!is_cpu() ) {
        throw GridError("-= is enabled onlin in cpu mode");
    }
#endif

    validate_dims(p);

    for(size_t i = 0; i < _n; ++i) {
        double *drow = _data + i * _m;
        const double *srow = p._data + i * _m;
        for(size_t j = 0; j < _m; ++j) {
            drow[j] -= srow[j];
        }
    }

    return *this;
}

Grid & Grid::operator *= (const double k)
{
#ifdef WITH_CUDA
    if(!is_cpu() ) {
        throw GridError("*= is enabled onlin in cpu mode");
    }
#endif

    for(size_t i = 0; i < _n; ++i) {
        double *drow = _data + i * _m;
        for(size_t j = 0; j < _m; ++j) {
            drow[j] *= k;
        }
    }

    return *this;
}

Grid & Grid::operator /= (const double k)
{
#ifdef WITH_CUDA
    if(!is_cpu() ) {
        throw GridError("/= is enabled onlin in cpu mode");
    }
#endif

    for(size_t i = 0; i < _n; ++i) {
        double *drow = _data + i * _m;
        for(size_t j = 0; j < _m; ++j) {
            drow[j] /= k;
        }
    }

    return *this;
}

Grid operator + (const Grid &a, const Grid &b)
{
    return Grid(a) += b;
}

Grid operator - (const Grid &a, const Grid &b)
{
    return Grid(a) -= b;
}

Grid operator * (const Grid &p, const double k)
{
    return Grid(p) *= k;
}

Grid operator * (const double k, const Grid &p)
{
    return Grid(p) *= k;
}

#ifdef WITH_CUDA
void Grid::to_gpu(bool copy)
{
    if(copy) {
        if(cudaDeviceSynchronize() != cudaSuccess || cudaMemcpy(_gpu_data, _cpu_data, _n * _m * sizeof(_data[0]), cudaMemcpyHostToDevice) != cudaSuccess || cudaDeviceSynchronize() != cudaSuccess) {
            throw GridError("cuda memcpy [host2device] failure");
        }
    }
    _data = _gpu_data;
}

void Grid::to_cpu(bool copy)
{
    if(copy) {
        if(cudaDeviceSynchronize() != cudaSuccess || cudaMemcpy(_cpu_data, _gpu_data, _n * _m * sizeof(_data[0]), cudaMemcpyDeviceToHost) != cudaSuccess || cudaDeviceSynchronize() != cudaSuccess) {
            throw GridError("cuda memcpy [device2host] failure");
        }
    }
    _data = _cpu_data;
}
#endif
