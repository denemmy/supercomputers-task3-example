#pragma once

#include <string>

#if __cplusplus > 199711L
 #include <cstdint>
#else
 #define noexcept throw()
 #define override
 #define nullptr NULL
#endif

class GridError: public std::exception {
    std::string _what;
public:
    GridError(const std::string &what): _what(what) {}
#if __cplusplus > 199711L
    ~GridError() = default;
#else
    ~GridError() noexcept;
#endif
    const char * what() const noexcept override { return _what.c_str(); }
};

class Grid
{
#ifdef WITH_CUDA
    double *_gpu_data;
    double *_cpu_data;
#endif
    double *_data;
    std::size_t _n, _m;
protected:
    bool check_dims(const Grid &p) const noexcept;
    void validate_dims(const Grid &p) const;
public:
    Grid();
    Grid(std::size_t n, std::size_t m, const double default_value = 0.0);
    virtual ~Grid();

    Grid(const Grid &p);
#if __cplusplus > 199711L
    Grid(Grid &&p);
#endif
    void swap(Grid &p) noexcept;

    Grid & operator = (const Grid &p);
#if __cplusplus > 199711L
    Grid & operator = (Grid &&p);
#endif

    const size_t n() const { return _n; }
    const size_t m() const { return _m; }
    std::size_t size() const { return _n * _m; }

    const double * data() const { return _data; }
    double * data() { return _data; }

    const double * operator [](const std::size_t i) const { return _data + i * _m; }
    double * operator [](const std::size_t i) { return _data + i * _m; }

    Grid & operator += (const Grid &p);
    Grid & operator -= (const Grid &p);
    Grid & operator *= (const double k);
    Grid & operator /= (const double k);

#ifdef WITH_CUDA
    bool is_gpu() const { return _data == _gpu_data; };
    bool is_cpu() const { return _data == _cpu_data; };
    void to_gpu(bool copy = true);
    void to_cpu(bool copy = true);
#endif
};

Grid operator + (const Grid &a, const Grid &b);
Grid operator - (const Grid &a, const Grid &b);
Grid operator * (const Grid &p, const double k);
Grid operator * (const double k, const Grid &p);
