#include "vector.h"
#include "memtools.h"

#include <cstdlib>
#include <cstring>

#ifdef WITH_CUDA
#include <cuda_runtime.h>
#endif

using std::size_t;
using std::free;

#if __cplusplus <= 199711L
VectorError::~VectorError() noexcept {
}
#endif

bool Vector::check_dims(const Vector &p) const noexcept
{
    return _n == p._n;
}

void Vector::validate_dims(const Vector &p) const
{
    if(!check_dims(p)) {
        throw VectorError("size mismatch");
    }
}

Vector::Vector():
    _data(nullptr), _n(0)
{
#ifdef WITH_CUDA
    _cpu_data = nullptr;
    _gpu_data = nullptr;
#endif
}

Vector::Vector(size_t n, const double default_value):
    _data(nullptr), _n(n)
{
#ifdef WITH_CUDA
    if(cudaMalloc(&_gpu_data, _n * sizeof(_data[0])) != cudaSuccess || cudaDeviceSynchronize() != cudaSuccess) {
        throw VectorError("cuda allocation failure");
    }
#endif
    _data = (double *)aligned_malloc(32, _n * sizeof(_data[0]));
    for(size_t i = 0; i < _n; ++i) {
        _data[i] = default_value;
    }
#ifdef WITH_CUDA
    _cpu_data = _data;
    to_gpu();
    _data = _cpu_data;
#endif
}

Vector::~Vector()
{
    if(_data) {
#ifdef WITH_CUDA
        cudaDeviceSynchronize();
        cudaFree(_gpu_data);
        _gpu_data = nullptr;
        _data = _cpu_data;
        _cpu_data = nullptr;
#endif
        free(_data);
        _data = nullptr;
    }
}

Vector::Vector(const Vector &p):
    _n(p._n), _data(nullptr)
{
#ifdef WITH_CUDA
    if(cudaMalloc(&_gpu_data, _n * sizeof(_data[0])) != cudaSuccess || cudaDeviceSynchronize() != cudaSuccess) {
        throw VectorError("cuda allocation failure");
    }
    if(cudaMemcpy(_gpu_data, p._gpu_data, _n * sizeof(_data[0]), cudaMemcpyDeviceToDevice) != cudaSuccess || cudaDeviceSynchronize() != cudaSuccess) {
        throw VectorError("cuda memcpy failure");
    }
    _cpu_data = (double *)aligned_malloc(32, _n * sizeof(_data[0]));
    memcpy(_cpu_data, p._cpu_data, _n * sizeof(_data[0]));
    if(p.is_cpu()) {
        _data = _cpu_data;
    } else {
        _data = _gpu_data;
    }
#else
    _data = (double *)aligned_malloc(32, _n * sizeof(_data[0]));
    memcpy(_data, p._data, _n * sizeof(_data[0]));
#endif
}

#if __cplusplus > 199711L
Vector::Vector(Vector &&p):
    _n(p._n), _data(p._data)
{
    p._n = 0;
    p._data = nullptr;
#ifdef WITH_CUDA
    _gpu_data = p._gpu_data;
    _cpu_data = p._cpu_data;
    p._gpu_data = nullptr;
    p._cpu_data = nullptr;
#endif
}
#endif

void Vector::swap(Vector &p) noexcept
{
    std::swap(_n, p._n);
    std::swap(_data, p._data);
#ifdef WITH_CUDA
    std::swap(_gpu_data, p._gpu_data);
    std::swap(_cpu_data, p._cpu_data);
#endif
}

Vector & Vector::operator = (const Vector &p)
{
    Vector tmp(p);
    this->swap(tmp);
    return *this;
}

#if __cplusplus > 199711L
Vector & Vector::operator = (Vector &&p)
{
    this->swap(p);
    return *this;
}
#endif

#ifdef WITH_CUDA
void Vector::to_gpu(bool copy)
{
    if(copy) {
        if(cudaDeviceSynchronize() != cudaSuccess || cudaMemcpy(_gpu_data, _cpu_data, _n * sizeof(_data[0]), cudaMemcpyHostToDevice) != cudaSuccess || cudaDeviceSynchronize() != cudaSuccess) {
            throw VectorError("cuda memcpy [host2device] failure");
        }
    }
    _data = _gpu_data;
}

void Vector::to_cpu(bool copy)
{
    if(copy) {
        if(cudaDeviceSynchronize() != cudaSuccess || cudaMemcpy(_cpu_data, _gpu_data, _n * sizeof(_data[0]), cudaMemcpyDeviceToHost) != cudaSuccess || cudaDeviceSynchronize() != cudaSuccess) {
            throw VectorError("cuda memcpy [device2host] failure");
        }
    }
    _data = _cpu_data;
}
#endif
